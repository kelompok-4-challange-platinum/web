Feature: Accept Offer
    Given user is in Home page

@positive_case
    Scenario: C028 User accept the offer by status
        When user see the offered from the buyer
        And user click accept button
        Then user click status button
        And user can change status to Berhasil terjual

@positive_case
    Scenario: C029 User accept the offer
        When user see the offered from the buyer
        And user click accept button
        Then user click Hubungi di WA button
        And user redirected to Whatsapp