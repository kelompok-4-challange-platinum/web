Feature: Register
  Background: User wants to Register
    Given User already on Register page

@positive_case
    Scenario: C002-User success Register
        When User fill nama and valid credential after that click button Daftar
        Then User will successfully Register and direct to Homepage

@negative_case
    Scenario: C003-User not success Register
        When hen User fill nama and invalid credential after that click button Daftar
        Then User not success Register with message Email has already taken
        And User redirect to Register page
     