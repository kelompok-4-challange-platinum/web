Feature: Reject Offer
    Given user is in Home page

@positive_case
    Scenario: C030 User reject the offer by status
        When user see the offered from the buyer
        And user click accept button
        Then user click status button
        And user can change status to Batalkan transaksi

@positive_case
    Scenario: C031 User reject the offer
        When user see the offered from the buyer
        Then user click reject button
        And user can see the status is Penawaran produk ditolak
        
