Feature: Edit Product on Secondhand Website
Background:
    Given the user is logged in to their account
    When on the homepage, user clicks the product list menu
    And user clicks on the product they want to manage
    And user's product detail page is displayed
    And user clicks Edit button to edit the product
  	And the website displays required fields for editing the product

@positive_case
    Scenario: C024-User wants to edit an existing product with valid details
    When there exists a product with the following details:
    | Field          | Value                                                                                                                                   |
    | Nama Produk    | Group4_Laptop Asus                                                                                                                      | 
    | Harga Produk   | 6000000                                                                                                                                 |
    | Kategori       | 4                                                                                                                                       |
    | Deskripsi      | High-quality electronics                                                                                                                |
    | Gambar Produk  | /Users/syaviraalatas/REZA/Bootcamp Binar Academy/QAE Quality Assurance Engineer/Challange Level Platinum/katalon/Images/laptop_asus.jpg |
    And user modifies the product description to "High-performance laptop"
    And user saves the changes by clicking the "Terbitkan" button
    Then the product should be edited successfully
  

@negative_case
    Scenario: C025-User wants to edit an invalid price on an existing product with non-numeric characters
    When there exists a product with the following details:
    | Field          | Value                                                                                                                                   |
    | Nama Produk    | Group4_Laptop Asus                                                                                                                      | 
    | Harga Produk   | 6000000                                                                                                                                 |
    | Kategori       | 4                                                                                                                                       |
    | Deskripsi      | High-quality electronics                                                                                                                |
    | Gambar Produk  | /Users/syaviraalatas/REZA/Bootcamp Binar Academy/QAE Quality Assurance Engineer/Challange Level Platinum/katalon/Images/laptop_asus.jpg |
    And user attempts to modify the product price to "6000000ab"
    Then the product should be not edited