Feature: Product Offer

    To ensure that User as buyer can or can not make an offer

    Background: buyer offering the product
        When buyer click product
        And buyer clik "Saya tertarik dan ingin nego" button
        And buyer input price
        And buyer click "Kirim" button
    
    @positive_case
    Scenario: C018-buyer can make an offer with valid input
        Given buyer already sign-in
        And buyer already completed the Profile
        Then buyer can offer the product
        And  "Saya tertarik dan ingin nego" button change to "Menunggu Respon Penjual"
    
    @negative_case
        Scenario: C019-buyer cannot make an offer when the profile is not completed
        Given buyer already sign-in
        And buyer has not completed the Profile
        Then buyer can not offer the product
        And buyer redirected to Edit Profile page
    
        Scenario: C020-buyer cannot make an offer when buyer not signed in
        Given buyer not sign-in
        Then buyer can not offer the product
        And buyer redirected to Sign-in page
        And buyer can see an error message "You need to sign in or sign up before continuing"

