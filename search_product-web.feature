Feature: Search Product

    To ensure that User can or can not search the product

    Background: user access the website
        Given user access the web url
    
    @positive_case
    Scenario: C013-user can see the search result with valid keyword
        When user input valid keyword in Search box on Homepage and click Enter
        Then user will see product with valid keyword on search result page

    Scenario: C015-user can see the search result with Category
        When user click one Category button in "Telusuri Kategori" section on Homepage
        Then user will see product with the same category on Homepage
    
    @negative_case
    Scenario: C016-user can not see the search result with wrong keyword
        When user input "<conditions>" keyword in Search box on Homepage and click Enter
        Then user will see empty result on search result page
    
    Scenario: C017-user can not see the search result with empty keyword
        When user input "<conditions>" keyword in Search box on Homepage and click Enter
        Then user will stay on the Homepage
            Examples:
            | case_id | conditions |
            | C016    | wrong      |
            | C017    | empty      |
