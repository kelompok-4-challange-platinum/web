Feature: Login
  Background: User wants to Login
    Given User already in login page

@positif_case
  Scenario: C005-User success Login
    When User input valid credential and click button Masuk
    Then User successfully Login
    And User redirect to Homepage
        
@negative_case
    Scenario: C006-User not success Login
        When User input invalid credential and click button Masuk
        Then User not success Login and get message Invalid email and password
        And User redirect to Login page



