Feature: Add Product on Secondhand Website
Background:
    Given the user is logged in to their account
    And user clicks "+ Jual" button to add a product on the homepage
    And the website displays required fields for adding a new product
    Then the user should be able to access and fill in the required information for the product

@positive_case
    Scenario: C021-User wants to add a product by entering valid product details
    When user enters valid product details in the required fields:
    | Field          | Value                                                                                                                                   |
    | Nama Produk    | Group4_Laptop Asus                                                                                                                      | 
    | Harga Produk   | 6000000                                                                                                                                 |
    | Kategori       | 4                                                                                                                                       |
    | Deskripsi      | High-quality electronics                                                                                                                |
    | Gambar Produk  | /Users/syaviraalatas/REZA/Bootcamp Binar Academy/QAE Quality Assurance Engineer/Challange Level Platinum/katalon/Images/laptop_asus.jpg |      
    Then the product is successfully

@positive_case
    Scenario: C022-User wants to preview a product before publishing it on the website.
    When user enters valid product details in the required fields:
    | Field          | Value                                                                                                                                  |
    | Nama Produk    | Smartphone                                                                                                                             | 
    | Harga Produk   | 3000000                                                                                                                                |
    | Kategori       | 4                                                                                                                                      |
    | Deskripsi      | High-quality smartphone                                                                                                                |
    | Gambar Produk  | /Users/syaviraalatas/REZA/Bootcamp Binar Academy/QAE Quality Assurance Engineer/Challange Level Platinum/katalon/Images/smartphone.jpg |
    And user clicks on the "Preview" button to preview the product
    Then the product is previewed successfully

@negative_case
    Scenario: C023-User wants to add a product but leaves the product title field blank.
    When user enters valid product details in the required fields:
    | Field          | Value                                                                                                                             |
    | Nama Produk    |                                                                                                                                   | 
    | Harga Produk   | 1500000                                                                                                                           |
    | Kategori       | 4                                                                                                                                 |
    | Deskripsi      | Ergonomic mouse                                                                                                                   |
    | Gambar Produk  | /Users/syaviraalatas/REZA/Bootcamp Binar Academy/QAE Quality Assurance Engineer/Challange Level Platinum/katalon/Images/mouse.jpg |
    And user clicks on the "Terbitkan" button to add the product
    Then the product is not added